.PHONY: build clean test run

IMAGE_REGISTRY ?= registry.gitlab.com/iwaseatenbyagrue
IMAGE_TAG ?= latest

build: bin/landing-linux-amd64 bin/landing-linux-arm bin/landing-linux-arm64 bin/landing-darwin-amd64 bin/landing-darwin-arm64
	rm -f bin/SHA256SUM
	cd bin && openssl sha256 -r landing-* > SHA256SUM

fragments = $(subst -, ,$@)
GOOS = $(word 2,$(fragments))
GOARCH = $(word 3,$(fragments))
bin/landing-%: main.go $(shell find assets -type f)
	mkdir -p bin
	GOOS=$(GOOS) GOARCH=$(GOARCH) CGO_ENABLED=0 go build -o bin/landing-$* -ldflags  '-w -s' main.go

clean:
	rm -rf bin

run:
	go run main.go -content assets

test:
	go fmt $(shell go list ./... | grep -v vendor/)
	go vet $(shell go list ./... | grep -v vendor/)
	go test -cover -race $(shell go list ./... | grep -v vendor/)


image: build
	docker build --tag $(IMAGE_REGISTRY)/landing:$(IMAGE_TAG) .
