# landing

A minimal, configurable home/landing page.

This project was inspired by other home page projects:

* https://github.com/bastienwirtz/homer
* https://dashy.to/

It aims to provide a smaller, faster, and simpler alternative to those projects.

## Features

* standalone server binary, only configuration is needed.
* simple `YAML` configuration format.
* display WIFI QR code (thanks to [`qrcodejs`](https://github.com/davidshimjs/qrcodejs)).
* small static binary (under 10MB by default), small runtime footprint.
* supply your own assets and templates at build or run time.
* support path prefix in URLs (e.g. expose on subpath).
* tiny codebase, easy to hack, fix, and extend.

## Installation

You can download pre-built static binaries for various platforms from the [releases page](https://gitlab.com/iwaseatenbyagrue/landing/-/releases).

These binaries can be run as-is, and require no additional software.

If you have `go` installed locally, you can run:

```
go install gitlab.com/iwaseatenbyagrue/landing@latest
```

You can control the install location by using the `GOBIN` environment variable:

```
sudo GOBIN=/usr/bin go install gitlab.com/iwaseatenbyagrue/landing@latest
```

### Requirements

The following requirements are *only* needed to build this project from source:

* go 1.16+
* GNU `make`

Run `make build` - the binaries and a checksum file will be created under `bin/`.

## Usage

`landing`'s configuration can be provided via a file, or directly from `stdin`.

### Quick start


#### Create a configuration file

By default, `landing` will look for a configuration file called `landing.yaml` in the working directory.

We can create a basic config file by running:

```
cat > landing.yaml <<EOF
title: My landing page
# Define custom header text (the value of 'title' is used by default).
header: Welcome to landing

# The wifi section is optional.
wifi:
    ssid: My WIFI
    password: my-wifi-password

sections:
    - name: Useful links
      items:
      - name: hacker news
        description: Tech news and more
        url: https://news.ycombinator.com
    - name: TODO
      items:
      - name: Shopping
        description: milk (2l), eggs (6), butter (250g), sugar (500g)
EOF
```

#### Run

```
landing
```

If you would prefer to save your configuration file under a diferent name or location, you can point `landing` to it:

```
landing --config path/to/config.yaml
```

You can also pipe configuration via `stdin`:

```
cat path/to/config.yaml | landing --config -
```

#### Help

You can check all supported options by running:

```
landing --help
```

### Run as a service

A systemd service unit for running `landing` as a user service is available at `init/landing.service`.

By default, this will load a configuration file from `$HOME/landing/config.yaml` for the user running it.

To run as a user service:
* `sudo GOBIN=/usr/bin go install gitlab.com/iwaseatenbyagrue/landing@latest` (or install to e.g. `$HOME/bin` if you prefer to not use `sudo`.)
* (optional) ensure your user's services run at boot (and/or after you log out from the system) by running `sudo loginctl enable-linger <USERNAME>`.
* copy `init/landing.service` to `/$HOME/.config/systemd/user/landing.service`
* (optional) create a configuration file under `$HOME/landing/config.yaml`
* (optional) create an environment file to listen on a different address, or load configuration from a different location under `$HOME/landing/env`
* `systemctl --user enable --now landing.service`

### Custom address

Listen on localhost only, port 9999:

```
landing --address 'localhost:9999'
```

### Custom content

Use your own static assets and templates on the fly:

```
landing --content path/to/my/content
```

Alternatively, you can fork this repo, and replace the JS, CSS and HTML template assets under `assets/`.
You can then build and ship binaries embedding your custom content.
See [requirements](#requirements) for further information.

## License

[MIT](https://choosealicense.com/licenses/mit/)