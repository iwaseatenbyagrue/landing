package main

import (
	"bytes"
	"embed"
	"errors"
	"flag"
	"fmt"
	"html/template"
	"io"
	"io/fs"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"gopkg.in/yaml.v2"
)

var (
	//go:embed assets/*
	defaultContent embed.FS
	configPath     = "landing.yaml"
	contentPath    = ""
	pathPrefix     = ""
	listenAddr     = ":12323"
)

type Config struct {
	Title    string     `yaml:"title"`
	Wifi     WifiConfig `yaml:"wifi"`
	Sections []Section  `yaml:"sections"`
	Header   string     `yaml:"header"`
	Footer   string     `yaml:"footer"`
	Prefix   string     `yaml:"prefix"`
}

type WifiConfig struct {
	Ssid     string `yaml:"ssid"`
	Password string `yaml:"password"`
	Hidden   bool   `yaml:"hidden"`
	Security string `yaml:"security"`
	Name     string `yaml:"name"`
}

// a.k.a groups
type Section struct {
	Name  string        `yaml:"name"`
	Items []SectionItem `yaml:"items"`
}

type SectionItem struct {
	Name        string       `yaml:"name"`
	Url         template.URL `yaml:"url"`
	Tag         string       `yaml:"tag"`
	Description string       `yaml:"description"`
	Icon        string       `yaml:"icon"`
}

func main() {
	flag.StringVar(&configPath, "config", configPath, "path to configuration file.")
	flag.StringVar(&listenAddr, "address", listenAddr, "address for server to listen on.")
	flag.StringVar(&pathPrefix, "prefix", "", "path prefix, use when exposing landing on a subpath.")
	flag.StringVar(&contentPath, "content", "", "optional path to alternative site content (static assets and templates).")
	flag.Parse()

	defaultContentFs, err := fs.Sub(defaultContent, "assets")

	if err != nil {
		log.Fatalf("[ERROR] default content has no assets directory: %v", err)
	}

	log.Printf("[INFO] load configuration file: %s", configPath)
	config, err := ConfigFromFile(configPath)

	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			log.Printf("[WARN] using fallback config: %v", err)
			config = GetFallbackConfig()
		} else {
			log.Fatalf("[ERROR] failed to open configuration file: %v", err)
		}
	}

	if pathPrefix != "" {
		(&config).Prefix = pathPrefix
	}

	var router *http.ServeMux

	if contentPath == "" {
		router, err = RouterFromFilesystem(defaultContentFs, config)
	} else {
		router, err = RouterFromFilesystem(os.DirFS(contentPath), config)
	}

	if err != nil {
		log.Fatalf("[ERROR] failed to create router: %v", err)
	}

	server := &http.Server{
		Addr:         listenAddr,
		Handler:      router,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  15 * time.Second,
	}

	log.Printf("[INFO] start server on address: %s", listenAddr)
	log.Fatalf("[ERROR] server failed to start: %v", server.ListenAndServe())
}

func GetFallbackConfig() Config {
	return Config{
		Title:  "Landing",
		Header: `Add your own configuration via "--config" flag.`,
	}
}

func RouterFromFilesystem(contentFs fs.FS, config Config) (*http.ServeMux, error) {
	router := http.NewServeMux()
	pathPrefix := strings.TrimSuffix(config.Prefix, "/")

	// Add route to our ServeMux, handling the pathPrefix if specified.
	addRouteHandler := func(route string, handler http.Handler) {
		if pathPrefix == "" {
			router.Handle(fmt.Sprintf("/%s", route), handler)
			return
		}
		router.Handle(fmt.Sprintf("%s/%s", pathPrefix, route), http.StripPrefix(fmt.Sprintf("%s", pathPrefix), handler))
	}

	// Static assets
	addRouteHandler("css/", http.FileServer(http.FS(contentFs)))
	addRouteHandler("js/", http.FileServer(http.FS(contentFs)))
	addRouteHandler("images/", http.FileServer(http.FS(contentFs)))

	// HTML page templates
	templates, err := fs.Sub(contentFs, "html")
	if err != nil {
		return router, err
	}
	addRouteHandler("", TemplateServer(templates, config))
	return router, err
}

// A `http.FileServer`-alike that returns rendered templates instead of raw files.
func TemplateServer(templateFs fs.FS, data any) http.Handler {
	return http.HandlerFunc(func(response http.ResponseWriter, request *http.Request) {
		templatePath := request.URL.Path

		if request.Method != http.MethodGet {
			http.Error(response, "allowed methods: GET", http.StatusMethodNotAllowed)
			return
		}

		if request.URL.Path == "/" {
			templatePath = "index.html"
		}

		pageTemplate, err := template.ParseFS(templateFs, templatePath)

		if err != nil {
			log.Printf("[ERROR] failed to parse template: %v", err)
			http.Error(response, "", http.StatusNotFound)
			return
		}

		if err := pageTemplate.Execute(response, data); err != nil {
			log.Printf("[ERROR] failed to render template: %v", err)
			http.Error(response, "failed to render page", http.StatusInternalServerError)
			return
		}
	})
}

func ConfigFromFile(path string) (config Config, err error) {
	var configBytes []byte

	if configPath == "-" {
		configBytes, err = ioutil.ReadAll(os.Stdin)
	} else {
		configBytes, err = os.ReadFile(configPath)
	}
	if err != nil {
		return config, err
	}
	config, err = ConfigFromReader(bytes.NewReader(configBytes))
	return config, err
}

func ConfigFromReader(reader io.Reader) (config Config, err error) {
	err = yaml.NewDecoder(reader).Decode(&config)
	return config, err
}
